#!/usr/bin/python2.7
#
# Master controller for FocusOps
# Runs as a Daemon
#

import sys
import time
from daemon import Daemon

class QueueLoop(Daemon):
	def run(self):
		#derp = 0
		while True:
			#if derp >= 10:
			#	break
			derp += 1
			print derp
			time.sleep(10)
		print "Loop done"

if __name__ == "__main__":
	daemon = QueueLoop('/tmp/commander.pid')
	if len(sys.argv) == 2:
		if 'start' == sys.argv[1]:
			daemon.start()
		elif 'stop' == sys.argv[1]:
			daemon.stop()
		elif 'restart' == sys.argv[1]:
			daemon.restart()
		else:
			print "Unknown Command"
			sys.exit(2)
		sys.exit(0)
	else:
		print "usage: %s start|stop|restart" % sys.argv[0]
		sys.exit(2)